PROJECT_LIST_SERVICES += pgsql

POSTGRES_DUMP_FILE ?= $(RESOURCES)/pgsql/dumpbase.sql
PGSQL_COMPOSE_FILE ?= $(RESOURCES)/pgsql/docker-compose
COMPOSE_FILE += -f $(PGSQL_COMPOSE_FILE).yml -f $(PGSQL_COMPOSE_FILE).$(ENV).yml
PRE_TARGETS_ENV += pgsql-env
TARGETS_HELP += pgsql-help
PRE_TARGETS_UP+=pgsql-up
PRE_TARGETS_START+=pgsql-start
PRE_TARGETS_STOP+=pgsql-stop
PRE_TARGETS_DOWN+=pgsql-down
PRE_TARGETS_DUMP+=pgsql-dump
PRE_TARGETS_RESTORE+=pgsql-restore

#POSTGRES_HOST ?= pgsql
POSTGRES_DB ?= example
POSTGRES_USER ?= example
POSTGRES_PASSWORD ?= example

pgsql-help:
	$(call SHOW_TITLE_HELP, PGSQL)
	$(call SHOW_CMD_HELP, pgsql-up) Запускаем окружение UP \(PostgreSQL\)
	$(call SHOW_CMD_HELP, pgsql-start) Запускаем/стартуем остановленное приложение \(PostgreSQL\)
	$(call SHOW_CMD_HELP, pgsql-restore) востановление кластера PostgreSQL\(залить дамп базы в кластер\)
	$(call SHOW_CMD_HELP, pgsql-dump) Сохраняем Dump таблицы для Developing
	$(call SHOW_CMD_HELP, pgsql-stop) Остановлеваем приложение \(PostgreSQL\)
	$(call SHOW_CMD_HELP, pgsql-rm) удаляем остановленное приложение \(PostgreSQL\)
	$(call SHOW_CMD_HELP, pgsql-cli) подключение консольным клиентом

pgsql-up:
	@docker-compose -f $(COMPOSE_FILE) up -d pgsql

pgsql-start:
	@docker-compose -f $(COMPOSE_FILE) start pgsql

pgsql-restore:
	$(call SHOW_TITLE_TARGET, PGSQL-RESTORE)
	@docker-compose -f $(COMPOSE_FILE) exec -T pgsql /usr/bin/psql -U $(POSTGRES_USER) $(POSTGRES_DB) < $(POSTGRES_DUMP_FILE)

pgsql-dump:
	$(call SHOW_TITLE_TARGET, PGSQL-DUMP)
	@docker-compose -f $(COMPOSE_FILE) exec pgsql pg_dump -U $(POSTGRES_USER) $(POSTGRES_DB) -O  > $(POSTGRES_DUMP_FILE)

pgsql-stop:
	@docker-compose -f $(COMPOSE_FILE) stop pgsql

pgsql-rm:
	@docker-compose -f $(COMPOSE_FILE) rm -s pgsql

pgsql-down: pgsql-stop pgsql-rm

pgsql-cli:
	@docker-compose -f $(COMPOSE_FILE) exec pgsql /usr/bin/psql -U $(POSTGRES_USER) $(POSTGRES_DB)

pgsql-clean:

pgsql-env:
	@echo "# ---- PGSQL ----" >> .env
	@echo "POSTGRES_DB="$(POSTGRES_DB) >> .env
	@echo "POSTGRES_USER="$(POSTGRES_USER) >> .env
	@echo "POSTGRES_PASSWORD="$(POSTGRES_PASSWORD) >> .env
	@#echo "POSTGRES_PASS="$(POSTGRES_PASS) >> .env
	@echo "" >> .env
